#! /usr/bin/python3

import advent
from collections import defaultdict

files = advent.initialize(2020,17,1,"Conway Cubes",["output"])

class BinaryGrid4D:
    def __init__(self):
        self.grid = set()

    def set(self, x, y, z, w):
        self.grid.add( (x,y,z,w) )

    def is_present(self, x, y, z, w):
        return (x,y,z,w) in self.grid

    def num_neighbors(self, x, y, z, w):
        count = 0
        for dx in (-1, 0, 1):
            for dy in (-1, 0, 1):
                for dz in (-1, 0, 1):
                    for dw in (-1, 0, 1):
                        if(dx,dy,dz,dw) == (0,0,0,0):
                            continue
                        count += (x+dx, y+dy, z+dz, w+dw) in self.grid
        return count

    def num_set(self):
        return len(self.grid)

    def ranges(self):
        if len(self.grid) == 0:
            return 0, 0, 0, 0, 0, 0, 0, 0
        arbitrary = next(iter(self.grid))
        minx = maxx = arbitrary[0]
        miny = maxy = arbitrary[1]
        minz = maxz = arbitrary[2]
        minw = maxw = arbitrary[3]

        for cell in self.grid:
            minx = min(minx, cell[0])
            maxx = max(maxx, cell[0])

            miny = min(miny, cell[1])
            maxy = max(maxy, cell[1])

            minz = min(minz, cell[2])
            maxz = max(maxz, cell[2])

            minw = min(minw, cell[2])
            maxw = max(maxw, cell[2])
        return (minx, miny, minz, minw, maxx, maxy, maxz, maxw)

    def __str__(self):
        ret = ""
        minx, miny, minz, minw, maxx, maxy, maxz, maxw = self.ranges()
        for w in range(minw, maxw+1):
            for z in range(minz, maxz+1):
                ret += f"z={z}, w={w}\n"
                for y in range(miny, maxy+1):
                    for x in range(minx, maxx+1):
                        if self.is_present(x,y,z,w):
                            ret += "#"
                        else:
                            ret += "."
                    ret += "\n"
                ret += "\n"
        return ret



def load_input(file):
    grid = BinaryGrid4D()
    z=0
    w=0
    for y, row in enumerate(file):
        for x, cell in enumerate(row.rstrip()):
            if cell == "#":
                grid.set(x,y,z,w)
    return grid

def cycle_grid(grid):
    out = BinaryGrid4D()
    minx, miny, minz, minw, maxx, maxy, maxz, maxw = grid.ranges()
    for x in range(minx-1, maxx+2):
        for y in range(miny-1, maxy+2):
            for z in range(minz-1, maxz+2):
                for w in range(minw-1, maxw+2):
                    neighbors = grid.num_neighbors(x,y,z,w)
                    if grid.is_present(x,y,z,w):
                        if neighbors in [2,3]:
                            out.set(x,y,z,w)
                            #print(x,y,z, f"survive {neighbors}")
                        #else:
                            #print(x,y,z, f"die {neighbors}")
                    else:
                        if neighbors == 3:
                            out.set(x,y,z,w)
                            #print(x,y,z, f"born {neighbors}")
                        #else:
                            #print(x,y,z, f"remain empty {neighbors}")
    return out


grid = load_input(files.input)
files.output.write("Before any cycles:\n\n")
files.output.write(str(grid)+"\n")

for cycle in range(6):
    grid = cycle_grid(grid)
    if cycle < 2:
        files.output.write(f"After {cycle+1} cycle{'s' if cycle != 0 else ''}:\n\n")
        files.output.write(str(grid)+"\n")

files.result.write(f"{grid.num_set()}\n")

