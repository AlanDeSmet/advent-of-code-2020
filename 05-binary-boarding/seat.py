#! /usr/bin/python3


def bisect(num_items, code, lowerchar, upperchar):
    min = 0
    max = num_items
    for char in code:
        assert char in [lowerchar, upperchar]
        if char == lowerchar:
            max = min + (max-min)//2
        elif char == upperchar:
            min = min + (max-min)//2
        else:
            assert False, f"Unknown letter {char}, only {lowerchar} or {upperchar} were expected"

    assert min == (max-1), f"Final range isn't two adjacent numbers: {min}-{max}"
    return min

def row_col_to_seat(row, col):
    return row*8+col

def pass_to_row_col(bpass):
    return (
            bisect(128, bpass[0:7], "F", "B"),
            bisect(8, bpass[7:10], "L", "R"),
            )

def pass_to_seat(bpass):
    row, col = pass_to_row_col(bpass)
    return row_col_to_seat(row, col)



def test(bpass, expected_row, expected_col, expected_seatnum):
    row, col = pass_to_row_col(bpass)
    assert row == expected_row
    assert col == expected_col
    seatnum = row_col_to_seat(row, col)
    assert seatnum == expected_seatnum

def main():
    test("FBFBBFFRLR",  44, 5, 357)
    test("BFFFBBFRRR",  70, 7, 567)
    test("FFFBBBFRRR",  14, 7, 119)
    test("BBFFBBFRLL", 102, 4, 820)



if __name__ == '__main__':
    import sys
    sys.exit(main())
