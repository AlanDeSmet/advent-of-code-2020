#! /usr/bin/python3

class Seating:
    def __init__(self, height, width):
        self.seats = []
        self.people = []
        for row in range(height):
            self.seats.append([False]*width)
            self.people.append([False]*width)

    def height(self):  return len(self.seats)

    def width(self):  return len(self.seats[0])

    def in_bounds(self, row, col):
        if row < 0: return False
        if col < 0: return False
        if row >= self.height(): return False
        if col >= self.width(): return False
        return True

    def validate_pos(self, row, col):
        assert row > 0 and row < (self.height()-1), \
            f"Row must be between 1 and {self.height()-2}, but is {row}"
        assert col > 0 and col < (self.width()-1), \
            f"Col must be between 1 and {self.width()-2}, but is {col}"

    def set_seat(self, row, col):
        self.validate_pos(row, col)
        self.seats[row][col] = True
        
    def set_person(self, row, col):
        self.validate_pos(row, col)
        assert self.seats[row][col] == True, f"Trying to put a person in {row},{col}, but there is no seat"
        self.people[row][col] = True

    def __str__(self):
        ret = ""
        for row in range(self.height()):
            for col in range(self.width()):
                if self.people[row][col]:
                    ret += "#"
                elif self.seats[row][col]:
                    ret += "L"
                else:
                    ret += "."
            ret += "\n"
        return ret

    def copy_seats_only(self):
        seating = Seating(self.height(), self.width())
        seating.seats = []
        for row in self.seats:
            seating.seats.append(row[:])
        return seating

    def count_occupied_neighbors(self, row, col):
        return (
                self.people[row+1][col+1] + 
                self.people[row+1][col  ] + 
                self.people[row+1][col-1] + 

                self.people[row  ][col+1] + 
                self.people[row  ][col-1] + 

                self.people[row-1][col+1] + 
                self.people[row-1][col  ] + 
                self.people[row-1][col-1] 
                )

    def count_occupied_visible(self, row, col):
        dirs = [
                (+1,+1),
                (+1, 0),
                (+1,-1),
                ( 0,+1),
                ( 0,-1),
                (-1,+1),
                (-1, 0),
                (-1,-1),
                ]
        count = 0
        for dir in dirs:
            posrow = row + dir[0]
            poscol = col + dir[1]
            while True:
                if not self.in_bounds(posrow, poscol):
                    break
                if self.seats[posrow][poscol]:
                    count += self.people[posrow][poscol]
                    break
                posrow += dir[0]
                poscol += dir[1]
        return count

    def __eq__(self, other):
        assert self.width() == other.width()
        assert self.height() == other.height()

        for row in range(self.height()):
            for col in range(self.width()):
                assert self.seats[row][col] == other.seats[row][col]
                if self.people[row][col] != other.people[row][col]:
                    return False
        return True

    def count_occupied(self):
        count = 0
        for row in self.people:
            count += sum(row)
        return count


def load_seating(file):
    raw = file.read().rstrip().split("\n")
    seating = Seating(len(raw)+2, len(raw[0])+2)
    for row in range(len(raw)):
        for col in range(len(raw[0])):
            if raw[row][col] == "L":
                seating.set_seat(row+1, col+1)
            else:
                assert raw[row][col] == ".", \
                        f"Unknown input value {raw[row][col]}"
    return seating
