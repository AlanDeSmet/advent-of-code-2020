#! /usr/bin/python3

def are_adds_present(window, sum):
    window = set(window)
    for first in window:
        for second in window:
            if first == second: continue
            if (first+second) == sum:
                return True
    return False

def find_invalid_entry(window, lines):
    for linenum in range(len(window), len(lines)):
        newval = lines[linenum]
        if not are_adds_present(window, newval):
            return newval
        window.append(newval)
        window = window[1:]
    return None

